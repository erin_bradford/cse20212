// Erin Bradford, ebradfo2, puzzle class

#ifndef PUZZLE_H
#define PUZZLE_H
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

template<typename T> // templated class so can be wordoku or sudoku
class Puzzle {
		
	public:
		Puzzle(string); // Constructor
		void print();// display puzzle
		int move(int, int, T);// change value in specific spot, only works if valid;
		int full(); // checks if puzzle is completed;
	private:
		int size;		
		vector< vector<T> > vPuzzle;
};

template<typename T>
Puzzle<T>::Puzzle(string file_name){
	size =9;
	ifstream file;
	file.open(file_name.c_str());
	if(!file.is_open())
	{
		cout << "Problem opening file: " << file_name.c_str()<<endl;
	}
	T temp;
	for (int i =0; i<size; i++) {
		vPuzzle.push_back(vector<T>());
		for (int j=0; j<size; j++) {
			file >> temp;			
			vPuzzle[i].push_back(temp);
		}		
	}
}

template<typename T>
void Puzzle<T>::print(){
	for (int i =0; i<size; i++){
		for (int j=0; j<size; j++){
			cout << vPuzzle[i][j] << " ";
		}
		cout << endl;
	}
}

template<typename T>
int Puzzle<T>::move(int row, int col, T val){

	int r1, r2, r3, c1, c2, c3;
	if (row % 3 == 0){				// establish where to check for miniboard comparison rows
		r1 = row; r3 = row +2;
	}
	else if (row % 3 == 1){
		r1 = row-1; r3 = row +1;
	}
	else if (row % 3 == 2){
		r1 = row-2; r3 = row;
	}
	if (col % 3 == 0){				// establish where to check for miniboard comparison cols
		c1 = col; c3 = col +2;
	}
	else if (col % 3 == 1){
		c1 = col-1; c3 = col +1;
	}
	else if (col % 3 == 2){
		c1 = col-2; c3 = col;
	}

	for (int a = r1; a<=r3; a++){ //check miniboard for conflicting val
		for (int b = c1; b<=c3; b++){
			if (vPuzzle[a][b] == val){
				cout << "Conflict in minisquare; couldn't place" << endl;
				return 0;
			}
		}
	}
	
	for (int c = 0; c<9; c++){ //check row and colfor conflicting val
		if(vPuzzle[row][c] == val){			
			cout << "Conflict in row; couldn't place" << endl;
			return 0;
		}
		else if (vPuzzle [c][col] == val){
			cout << "Conflict in column; couldn't place" << endl;
			return 0;
		}
	}
	
	vPuzzle[row][col] = val;
	return 1; // passed all tests without conflicts
}

template<typename T>
int Puzzle<T>::full(){
	for (int i =0; i<size; i++){
		for (int j=0; j<size; j++){	
			if (vPuzzle[i][j] == '0' || vPuzzle[i][j] == 0) return 0; //check if any squares are empty
		}
	}
	return 1; //if not board is full
}

#endif
