//Erin Bradford, ebradfo2, puzzle display

#include <iostream>
#include <vector>
#include "Puzzle.h"

using namespace std;

int main(int argc, char**argv){
	string file;
	int row, col, val;
	file = argv[1];
	Puzzle<int> sudoku(file);
	string file2;
	file2 = argv[2];
	Puzzle<char> wordoku(file2);
	cout << "Wordoku game" << endl;
	wordoku.print();
	cout << "Play sudoku game!"<< endl;
	sudoku.print();
	while (!sudoku.full()){
		cout << "Which row? (1-9)  ";
		cin >> row;
		cout << "which column? (1-9)  ";
		cin >> col;
		cout << "What value? (1-9)  ";
		cin >> val;
		sudoku.move(row-1, col-1, val);
		sudoku.print();
	}
	cout << "You win!"<< endl;
	
}	

